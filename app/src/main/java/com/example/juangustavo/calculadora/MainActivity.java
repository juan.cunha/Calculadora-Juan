package com.example.juangustavo.calculadora;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    int a = 0;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void adicionarNumero(View view) {
        String numero = ((TextView) view).getText().toString();
        TextView resultado = (findViewById(R.id.calcular));
        resultado.setText(resultado.getText() + numero);

    }

    public void soma(View view) {
        TextView soma = (findViewById(R.id.calcular));
        TextView calculo = (findViewById(R.id.calculo));
        calculo.setText(soma.getText());
        soma.setText("");
        a = 1;
    }

    public void subtracao(View view) {
        TextView subtracao = (findViewById(R.id.calcular));
        TextView calculo = (findViewById(R.id.calculo));
        calculo.setText(subtracao.getText());
        subtracao.setText("");
        a = 2;
    }

    public void multiplicacao(View view) {
        TextView multiplicacao = (findViewById(R.id.calcular));
        TextView calculo = (findViewById(R.id.calculo));
        calculo.setText(multiplicacao.getText());
        multiplicacao.setText("");
        a = 3;
    }

    public void divisao(View view) {
        TextView divisao = (findViewById(R.id.calcular));
        TextView calculo = (findViewById(R.id.calculo));
        calculo.setText(divisao.getText());
        divisao.setText("");
        a = 4;
    }

    public void raiz(View view) {
        TextView raizquadrada = (findViewById(R.id.calcular));
        TextView calculo = (findViewById(R.id.calculo));
        double num1 = Double.parseDouble(raizquadrada.getText().toString());
        num1 = Math.sqrt(num1 );
        String num = String.valueOf(num1);
        calculo.setText(num);
        raizquadrada.setText("0");
    }

    public void resultado(View view) {
        TextView calcular = (findViewById(R.id.calcular));
        TextView calculo = (findViewById(R.id.calculo));
        if (a == 1) {
            String num;
            double num1 = Double.parseDouble(calcular.getText().toString());
            // resgata o valor digitado no segundo campo
            double num2 = Double.parseDouble(calculo.getText().toString());
            // o resultado da soma dos dois numeros
            double res = num1 + num2;
            num = String.valueOf(res);
            calculo.setText(num);
            calcular.setText("0");
        }
        if (a == 2) {
            String num;
            double num1 = Double.parseDouble(calcular.getText().toString());
            // resgata o valor digitado no segundo campo
            double num2 = Double.parseDouble(calculo.getText().toString());
            // o resultado da soma dos dois numeros
            double res = num1 - num2;
            num = String.valueOf(res);
            calculo.setText(num);
            calcular.setText("0");
        }
        if (a == 3) {
            String num;
            double num1 = Double.parseDouble(calcular.getText().toString());
            // resgata o valor digitado no segundo campo
            double num2 = Double.parseDouble(calculo.getText().toString());
            // o resultado da soma dos dois numeros
            double res = num1 * num2;
            num = String.valueOf(res);
            calculo.setText(num);
            calcular.setText("0");
        }
        if (a == 4) {
            String num;
            double num1 = Double.parseDouble(calcular.getText().toString());
            double num2 = Double.parseDouble(calculo.getText().toString());
            double res = num2 / num1;
            num = String.valueOf(res);
            calculo.setText(num);
            calcular.setText("0");
        }



    }

    public void cancelar(View view) {
        TextView calcular = (findViewById(R.id.calcular));
        TextView calculo = (findViewById(R.id.calculo));
        calcular.setText("");
        calculo.setText("0");
    }

    public void apagarnum(View view) {
        TextView calcular = (findViewById(R.id.calcular));
        calcular.setText("\b");
    }
}


